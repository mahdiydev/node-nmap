"use strict";

import { HostAddress, IReport, IReportData } from "./types";

const nmap = require("libnmap");

interface CalculateSecurityScoreResponse {
  address: HostAddress[];
  closedPortScore: number;
  openPortScore: number;
}

interface CalculatedResult {
  name: string;
  score: CalculateSecurityScoreResponse[];
}

export function scanAndCalcScore(url: string) {
  const opts = {
    range: [url, "192.168.0.0/26"],
  };

  const calculatedResult = new Promise((res, rej) =>
    nmap.scan(opts, function (err: Error, report: IReportData) {
      if (err) throw new Error(err.message);
      const calculatedResult: CalculatedResult[] = [];

      for (const [key, value] of Object.entries(report)) {
        calculatedResult.push({
          name: key,
          score: calculateSecurityScore(value),
        });
      }

      res(calculatedResult);
    })
  );

  return calculatedResult;
}

function calculateSecurityScore(
  openPorts: IReport
): CalculateSecurityScoreResponse[] {
  const scoredPorts = openPorts.host
    ? openPorts.host?.map((h) => {
        let closedPortScore = 0;
        let openPortScore = 0;

        h.ports.map((p) => {
          p.port.map((portInfo) => {
            portInfo.state.map((s) => {
              if (s.item.state === "closed") {
                closedPortScore += 5;
              } else if (s.item.state === "open") {
                openPortScore += 5;
              }
            });
          });
        });

        return { address: h.address, closedPortScore, openPortScore };
      })
    : [
        {
          address: [] as HostAddress[],
          closedPortScore: 0,
          openPortScore: 0,
        },
      ];

  return scoredPorts;
}
