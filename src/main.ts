import { Application, Request, Response } from "express";

import express from "express";
import { scanAndCalcScore } from "./scanAndClaculate";

const app = express() as Application;
const PORT = 4040;

app.use(express.json());

app.get("/*", async (req: Request, res: Response) => {
  try {
    res.send(
      '<code>try to POST /scan<br/>Body: { "url": "google.com" }</code>'
    );
  } catch (error) {
    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.post("/scan", async (req: Request, res: Response) => {
  try {
    const { url } = req.body;
    if (!url) {
      res.json({ message: "url is required" });
      return;
    }
    res.json(await scanAndCalcScore(url));
  } catch (error) {
    console.log(error);

    res.status(500).json({ message: "Internal Server Error" });
  }
});

app.listen(PORT, () => {
  console.log(`Server is running on http://localhost:${PORT}`);
});
