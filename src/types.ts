/*
    I wrote types for libnmap. Because no body does.
*/

interface ReportItem {
  scanner: string;
  args: string;
  start: string;
  startstr: string;
  version: string;
  xmloutputversion: string;
}

interface Scaninfo {
  item: {
    type: string;
    protocol: string;
    numservices: string;
    services: string;
  };
}

interface Verbose {
  item: { level: string };
}

interface Finished {
  item: {
    time: string;
    timestr: string;
    summary: string;
    elapsed: string;
    exit: string;
  };
}

interface Hosts {
  item: { up: string; down: string; total: string };
}

interface Runstats {
  finished: Finished[];
  hosts: Hosts[];
}

interface Address {
  item: { addr: string; addrtype: string };
}

interface HosthintStatus {
  item: {
    state: string;
    reason: string;
    reason_ttl: string;
  };
}

interface Hosthint {
  status: HosthintStatus[];
  address: Address[];
  hostnames: string[];
}

interface Status {
  item: { state: string; reason: string; reason_ttl: string };
}

export interface HostAddress {
  item: { addr: string; addrtype: string };
}

interface HostPortsExtraportsExtrareasons {
  item: {
    reason: string;
    count: string;
    proto: "tcp" | "udp";
    ports: string;
  };
}

interface HostPortsExtraports {
  item: { state: string; count: string };
  extrareasons: HostPortsExtraportsExtrareasons[];
}

interface HostPortsPortState {
  item: {
    state: "closed" | "open";
    reason: string;
    reason_ttl: string;
  };
}

interface HostPortsPortService {
  item: { name: string; method: string; conf: string };
}

interface HostPortsPort {
  item: { protocol: "tcp" | "udp"; portid: string };
  state: HostPortsPortState[];
  service: HostPortsPortService[];
}

interface HostPorts {
  extraports: HostPortsExtraports[];
  port: HostPortsPort[];
}

interface HostTimes {
  item: { srtt: string; rttvar: string; to: string };
}

interface Host {
  item: { starttime: string; endtime: string };
  status: Status[];
  address: HostAddress[];
  hostnames: string[];
  ports: HostPorts[];
  times: HostTimes[];
}

export interface IReport {
  item: ReportItem;
  scaninfo: Scaninfo[];
  verbose: Verbose[];
  debugging: Verbose[];
  runstats: Runstats[];
  hosthint?: Hosthint[];
  host?: Host[];
}

export type IReportData = Record<string, IReport>;
