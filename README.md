# Node-nmap

build command:
```shell
npm run build
```

run application:
```shell
npm start
```

### Using Docker commands
create image:
```shell
docker build -t my-node-service .
```
run image:
```
docker run -d --restart=always -p 4040:4040 my-node-service
```


### Application usage
 - URL: http://localhost:4040/scan
 - Method: POST
 - Body:
```json
{ 
    "url": "google.com"
}
```
